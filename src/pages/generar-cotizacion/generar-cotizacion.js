var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Service } from '../../providers/service/service';
import { Images } from '../../providers/images/images';
/**
 * Generated class for the GenerarCotizacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var GenerarCotizacionPage = (function () {
    function GenerarCotizacionPage(service, alertCtrl, img, navCtrl, navParams) {
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.img = img;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.myDate = new Date().toISOString();
        this.accion = 'GUARDAR SINIESTRO';
        this.form = {
            //generales
            tipo: '',
            nombre: '',
            email: '',
            //auto
            dominio: '',
            aseguradora: '',
            anio: '',
            telefono: '',
            lugar: '',
            marca: '',
            modelo: '',
            fecha: '',
            combustible: '',
            image_auto: './assets/images/sin-imagen.png',
            //otros
            descripcion: '',
            //negocio
            metros_negocio: '',
            actividad_negocio: '',
            localidad_negocio: '',
            //art
            empresa_art: '',
            capitas_art: '',
            image_art: './assets/images/sin-imagen.png'
        };
    }
    GenerarCotizacionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GenerarCotizacionPage');
    };
    GenerarCotizacionPage.prototype.ionViewDidEnter = function () {
        console.log(' didenter GenerarSiniestroPage');
    };
    GenerarCotizacionPage.prototype.sendForm = function (form) {
        var _this = this;
        var ts = (new Date()).getTime();
        var doc = form;
        var shot = "data:image/jpeg;base64,";
        if (form.tipo == 'AUTO') {
            //otros
            doc.descripcion = null;
            //negocio
            doc.metros_negocio = null;
            doc.actividad_negocio = null;
            doc.localidad_negocio = null;
            //art
            doc.empresa_art = null;
            doc.capitas_art = null;
            doc.image_art = null;
            //fin
        }
        else if (form.tipo == 'NEGOCIO') {
            //auto
            doc.dominio = null;
            doc.aseguradora = null;
            doc.anio = null;
            doc.telefono = null;
            doc.lugar = null;
            doc.marca = null;
            doc.modelo = null;
            doc.fecha = null;
            doc.combustible = null;
            doc.image_auto = null;
            //otros
            doc.descripcion = null;
            //art
            doc.empresa_art = null;
            doc.capitas_art = null;
            doc.image_art = null;
            //fin
        }
        else if (form.tipo == 'ART') {
            //auto
            doc.dominio = null;
            doc.aseguradora = null;
            doc.anio = null;
            doc.telefono = null;
            doc.lugar = null;
            doc.marca = null;
            doc.modelo = null;
            doc.fecha = null;
            doc.combustible = null;
            doc.image_auto = null;
            //otros
            doc.descripcion = null;
            //negocio
            doc.metros_negocio = null;
            doc.actividad_negocio = null;
            doc.localidad_negocio = null;
        }
        else if (form.tipo == 'OTROS') {
            //auto
            doc.dominio = null;
            doc.aseguradora = null;
            doc.anio = null;
            doc.telefono = null;
            doc.lugar = null;
            doc.marca = null;
            doc.modelo = null;
            doc.fecha = null;
            doc.combustible = null;
            doc.image_auto = null;
            //negocio
            doc.metros_negocio = null;
            doc.actividad_negocio = null;
            doc.localidad_negocio = null;
            //art
            doc.empresa_art = null;
            doc.capitas_art = null;
            doc.image_art = null;
        }
        if (doc.image_auto != './assets/images/sin-imagen.png' || doc.image_art != './assets/images/sin-imagen.png') {
            doc._attachments = {};
        }
        doc._id = 'cotizacion' + ts;
        doc._rev = null;
        doc.fecha = ts;
        doc.empresa = sessionStorage.getItem('empresa');
        doc.type = 'cotizacion';
        if (doc.image_auto == './assets/images/sin-imagen.png') {
            doc.image_auto = null;
        }
        if (doc.image_art == './assets/images/sin-imagen.png') {
            doc.image_art = null;
        }
        if (doc.image_auto && doc.image_auto.length > 100) {
            var auto = doc.image_auto.replace(shot, '');
            doc._attachments.foto_auto = {
                content_type: 'image/jpeg',
                data: auto
            };
            doc.image_auto = null;
        }
        if (doc.image_art && doc.image_art.length > 100) {
            var art = doc.image_art.replace(shot, '');
            doc._attachments.foto_art = {
                content_type: 'image/jpeg',
                data: art
            };
            doc.image_art = null;
        }
        this.service.dataBase.put(doc).then(function (res) {
            console.log('doc:');
            console.log(res);
            var alert = _this.alertCtrl.create({
                title: 'COTIZACION ENVIADA',
                cssClass: 'contactoAlert',
                message: 'Uno de nuestros representantes se comunicara para asesorarte sobre tu nueva cotizacion!',
                buttons: [
                    {
                        text: 'GRACIAS',
                        role: 'ok',
                        handler: function () {
                            _this.navCtrl.push(HomePage);
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    ;
    GenerarCotizacionPage.prototype.takePic = function () {
        var _this = this;
        this.img.presentActionSheet().then(function (base64) {
            if (base64 == 'Selection cancelled.' || base64 == 'Camera cancelled.' || base64 === null || base64 == 'undefined' || base64.length <= 20) {
                console.log('Sin Imagen desde Camara/Galeria');
            }
            else {
                var obj = "data:image/jpeg;base64," + base64;
                if (_this.form.tipo == 'AUTO') {
                    _this.form.image_auto = obj;
                }
                else {
                    _this.form.image_art = obj;
                }
            }
        });
    };
    return GenerarCotizacionPage;
}());
GenerarCotizacionPage = __decorate([
    IonicPage({
        name: 'COTIZAR',
        segment: 'generar-cotizacion'
    }),
    Component({
        selector: 'page-generar-cotizacion',
        templateUrl: 'generar-cotizacion.html',
    }),
    __metadata("design:paramtypes", [Service, AlertController, Images, NavController, NavParams])
], GenerarCotizacionPage);
export { GenerarCotizacionPage };
//# sourceMappingURL=generar-cotizacion.js.map