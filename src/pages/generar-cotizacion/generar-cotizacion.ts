import { Component } from '@angular/core';
import { NavController, NavParams,AlertController,Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Service } from '../../providers/service/service';

import { Images } from '../../providers/images/images';

/**
 * Generated class for the GenerarCotizacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
//  */
// @IonicPage({
//   name: 'COTIZAR',
//   segment: 'generar-cotizacion'
// })
@Component({
  selector: 'page-generar-cotizacion',
  templateUrl: 'generar-cotizacion.html',
})
export class GenerarCotizacionPage {
  fecha:any;
  myDate:any = new Date().toISOString();
  form: any;
  accion: string = 'GUARDAR SINIESTRO';
  constructor(public platform: Platform, public service: Service, public alertCtrl:AlertController, public img: Images, public navCtrl: NavController, public navParams: NavParams) {
  	   
          this.form = {
                  //generales
	           	    tipo: '',
                  nombre: '',
                  email: '',

                  //auto
                  dominio: '', 
                  aseguradora: '', 
                  anio:'',
                  telefono: '', 
                  lugar: '', 
                  marca: '', 
                  modelo: '', 
                  fecha: '',
                  combustible: '',
                  image_auto: './assets/images/sin-imagen.png',

                  //otros
                  descripcion: '',
                  
                  //negocio
                  metros_negocio: '',
                  actividad_negocio:'',
                  localidad_negocio:'',

                  //art
                  empresa_art: '',
                  capitas_art:'',
                  image_art:'./assets/images/sin-imagen.png'
                 
          }

          this.platform.registerBackButtonAction(() => {
            this.backHome();
          })
  


  }


  backHome(){
    this.navCtrl.push(HomePage,{});
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GenerarCotizacionPage');

    if (localStorage.getItem('email') && localStorage.getItem('email') != 'default@seguros_atender') {
          this.form.nombre= localStorage.getItem('nombre');
          this.form.email= localStorage.getItem('email');
    }

  }
  
  ionViewDidEnter() {
    console.log(' didenter GenerarSiniestroPage');
  }

    sendForm(form){
     let ts = ( new Date() ).getTime();
     let doc = form;
     let shot = "data:image/jpeg;base64,";

          if (form.tipo == 'AUTO') {
                  //otros
                  doc.descripcion =  null; 
                  //negocio
                  doc.metros_negocio =  null;
                  doc.actividad_negocio = null;
                  doc.localidad_negocio = null;
                  //art
                  doc.empresa_art =  null;
                  doc.capitas_art = null;
                  doc.image_art = null;
                  //fin
      }else if(form.tipo == 'NEGOCIO'){
                  //auto
                  doc.dominio= null; 
                  doc.aseguradora= null; 
                  doc.anio = null;
                  doc.telefono= null; 
                  doc.lugar= null; 
                  doc.marca= null; 
                  doc.modelo= null; 
                  doc.fecha= null;
                  doc.combustible = null;
                  doc.image_auto = null;
                  //otros
                  doc.descripcion = null;
                  //art
                  doc.empresa_art = null;
                  doc.capitas_art = null;
                  doc.image_art = null;
                  //fin
      }else if(form.tipo == 'ART'){
                   //auto
                  doc.dominio= null; 
                  doc.aseguradora= null; 
                  doc.anio = null;
                  doc.telefono= null; 
                  doc.lugar= null; 
                  doc.marca= null; 
                  doc.modelo= null; 
                  doc.fecha= null;
                  doc.combustible = null;
                  doc.image_auto = null;
                  //otros
                  doc.descripcion = null;                                  
                  //negocio
                  doc.metros_negocio = null;
                  doc.actividad_negocio = null;
                  doc.localidad_negocio = null;
      }else if(form.tipo == 'OTROS'){
                  //auto
                  doc.dominio =  null; 
                  doc.aseguradora =  null; 
                  doc.anio = null;
                  doc.telefono =  null; 
                  doc.lugar =  null; 
                  doc.marca =  null; 
                  doc.modelo =  null; 
                  doc.fecha =  null;
                  doc.combustible =  null;
                  doc.image_auto = null;
                  //negocio
                  doc.metros_negocio =  null;
                  doc.actividad_negocio = null;
                  doc.localidad_negocio = null;
                  //art
                  doc.empresa_art =  null;
                  doc.capitas_art = null;
                  doc.image_art =null;                 
      }
     

      if (doc.image_auto != './assets/images/sin-imagen.png' || doc.image_art != './assets/images/sin-imagen.png' ) {
        doc._attachments = {};
      }
      
      doc._id = 'cotizacion'+ts;
      doc._rev = null;
      doc.fecha = ts;     
      doc.empresa = sessionStorage.getItem('empresa');
      doc.type = 'cotizacion';
      
     if (doc.image_auto == './assets/images/sin-imagen.png') {
         doc.image_auto = null;
     }

     if (doc.image_art == './assets/images/sin-imagen.png') {
        doc.image_art = null;
     }
     if (doc.image_auto && doc.image_auto.length > 100) {        
         let auto = doc.image_auto.replace(shot,'');
         doc._attachments.foto_auto = {
                                     content_type: 'image/jpeg',
                                     data: auto
                                   }
         doc.image_auto = null;
      }

     if (doc.image_art && doc.image_art.length > 100) { 
       let art = doc.image_art.replace(shot,'');
       doc._attachments.foto_art = {
                                     content_type: 'image/jpeg',
                                     data: art
                                   }
        doc.image_art = null;
      }
      
      this.service.dataBase.put(doc).then((res:any)=>{
                         console.log('doc:')
                         console.log(res)

                          let alert = this.alertCtrl.create({
                                    title: 'COTIZACION ENVIADA',
                                    cssClass: 'contactoAlert',
                                    message: 'Te vamos a llamar para asesorarte sobre tu nueva cotizacion',
                                    buttons: [
                                    {
                                        text: 'GRACIAS',
                                        role: 'ok',
                                        handler: () => {
                                          this.navCtrl.push(HomePage)
                                        }
                                    }
                                  ]
                                  });
        alert.present();
      })
  };

  takePic(){
    
        this.img.presentActionSheet().then((base64: any) => {
      

          if (base64 == 'Selection cancelled.' || base64 == 'Camera cancelled.' || base64 === null || base64 == 'undefined' || base64.length <= 20) {
           console.log('Sin Imagen desde Camara/Galeria')
         }else{              

           let obj = "data:image/jpeg;base64," + base64;
     
           if (this.form.tipo == 'AUTO') {
             this.form.image_auto = obj;
           }else{
             this.form.image_art = obj;
           }
      
        }               
      
      })

  }


}
