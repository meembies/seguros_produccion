import { Component } from '@angular/core';
import { NavController, NavParams,  Platform, AlertController } from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';
//import { Service } from '../../providers/service/service';
// import { SMS } from '@ionic-native/sms';
import { HomePage } from '../home/home';

import { Service } from '../../providers/service/service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController } from 'ionic-angular'
import { AndroidPermissions } from '@ionic-native/android-permissions';

var aseguradoras:any = []



/**
 * Generated class for the ListaAseguradorasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.





function myFunction() {
    .sort(function(a, b){return a - b});
    
}

 */

 
// @IonicPage({
//   name: 'ASEGURADORAS',
//   segment: 'lista-aseguradoras'
// })
@Component({
  selector: 'page-lista-aseguradoras',
  templateUrl: 'lista-aseguradoras.html',
})
export class ListaAseguradorasPage {
  
  aseguradoras: any;
  searchTerm :any = '';
  constructor(
        private androidPermissions: AndroidPermissions, 
        public platform: Platform, 
        public actionSheetCtrl: ActionSheetController, 
        private _sanitizer: DomSanitizer, 
        private pouch: Service, 
        private callNumber: CallNumber, 
        private socialSharing: SocialSharing, 
        private iab: InAppBrowser, 
        public alertCtrl: AlertController, 
        public navCtrl: NavController, 
        public navParams: NavParams) {
            this.platform.registerBackButtonAction(() => {
                this.backHome();
            })
  }

  backHome(){
    this.navCtrl.push(HomePage,{});
  }

  ionViewDidEnter() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaAseguradorasPage');

     let  id = sessionStorage.getItem('empresa');

      this.pouch.getDocumentByID(id).then((res: any)=>{ 
                   

           res.aseguradoras.sort((a, 
             b) =>{
               return a.orden - b.orden;

           });
           
           aseguradoras = res.aseguradoras;
           
           console.log(aseguradoras);
           

           this.aseguradoras =  aseguradoras;


       for (var i = 0; i < res.aseguradoras.length; i++) {
         this.setPic(id,res.aseguradoras[i].logo,i) 
       }
           this.setFilteredItems();         
     });

    }

  openOpt(a) {
    console.log(a);
   let actionSheet = this.actionSheetCtrl.create({
     title: a.nombre,
     buttons: [
       // {
       //   text: 'Llamar a la compañia',
       //   role: 'destructive',
       //   handler: () => {
       //     console.log('Destructive clicked');
       //     this.callGrua(a.tel)
       //   }
       // },
       {
         text: 'Solicitar grúa telefonicamente',
         handler: () => {
           console.log('grua clicked');
           this.callGrua(a.grua)
         }
       },
       {
         text: 'Solicitar grúa por SMS',
         handler: () => {
           console.log('sms clicked');
           if (a.msj) {
               this.smsGrua(a.msj)
           }else{
             alert('La compañia no posee cargado un umero para dicho servicio');
           }
           
         }
       // },
       // {
       //   text: 'Visitar sitio web',
       //   role: 'destructive',
       //   handler: () => {
       //     console.log('Destructive clicked');
       //     this.visitWeb(a.web)
       //   }
       },
       {
         text: 'Cancelar',

         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
 }
  
  visitWeb(web){
     let browser = this.iab.create(web,'_blank','location=true,hardwareback=true');
  }

  callGrua(number :any){

     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CALL_PHONE).then((result) =>{
       this.callNumber.callNumber(number, false).then( (res) =>{ console.log('calling') });
     },(err) =>{ 
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE)
      this.callGrua(number);
    });
         
  }

  setFilteredItems() {
        this.aseguradoras = this.filterItems(this.searchTerm);
  }

  filterItems(searchTerm){
        return aseguradoras.filter((item) => {
            return item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });     
  }

  smsGrua(msj){
    let number = msj.numero;
    let text = msj.texto;
    

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(
    (result) =>{

    this.socialSharing.shareViaSMS(text, number).then(() => {
                          // Success!
                          alert('Mensaje enviado correctamente.')

                        }).catch(() => {
                          // Error!
                          alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.')

                        });

    },(err) =>{ 
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS)
      this.smsGrua(msj)
    });
  }

  shareWhatsaps(item){
    var msj: string = 'Te paso los datos de la compañia ' + item.nombre+'. El numero de grua de dicha compañia es: ' + item.grua+'. Por ultimo si queres visitar su sitio web. Te comparto el link: '+item.web;

    //shareViaWhatsApp(message, image, url)

    this.socialSharing.shareViaWhatsApp(msj, null, null).then(() => {
                          // Success!
                          alert('La aseguradora se compartio correctamente.')

                        }).catch(() => {
                          // Error!
                          alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.')

                        });

  }

  shareViaMail(item){

             let prompt = this.alertCtrl.create({
              title: 'Enviar via Mail',
              message: "¿A quien desea enviarle los datos de la compañia seleccionada?",
              cssClass: 'contactoAlert',
              inputs: [
                {
                  name: 'email',
                  type: 'email',
                  placeholder: 'Ingese aqui el email del destinatario.'
                },
              ],
              buttons: [
                {
                  text: 'Enviar',
                  handler: data => {
                        // Share via email
                        var msj: string = 'Hago envio de los datos de la compañia ' + item.nombre+'. El numero de grua de dicha compañia es: ' + item.grua+'. Por ultimo si queres visitar su sitio web. Te comparto el link: '+item.web;
                        this.socialSharing.shareViaEmail(msj, 'Aseguradora Compartida', data.email).then(() => {
                          // Success!
                          alert('El mensaje se envio correctamente.');

                        }).catch(() => {
                          // Error!
                          alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.')

                        });

                  }
                },
                 {
                  text: 'Cancelar',
                  handler: data => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            });
            prompt.present();


      

  }


  setPic(id,name,i){
     console.log(id,name,i)
     this.pouch.getImage(id, name).then((url:any)=>{
         this.aseguradoras[i].logo = this._sanitizer.bypassSecurityTrustUrl(url);
     })
   }

}
