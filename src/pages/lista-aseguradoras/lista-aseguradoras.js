var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';
//import { Service } from '../../providers/service/service';
import { Service } from '../../providers/service/service';
import { DomSanitizer } from '@angular/platform-browser';
var aseguradoras = [];
/**
 * Generated class for the ListaAseguradorasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.





function myFunction() {
    .sort(function(a, b){return a - b});
    
}

 */
var ListaAseguradorasPage = (function () {
    function ListaAseguradorasPage(_sanitizer, pouch, callNumber, socialSharing, iab, alertCtrl, navCtrl, navParams) {
        this._sanitizer = _sanitizer;
        this.pouch = pouch;
        this.callNumber = callNumber;
        this.socialSharing = socialSharing;
        this.iab = iab;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.searchTerm = '';
    }
    ListaAseguradorasPage.prototype.ionViewDidEnter = function () {
    };
    ListaAseguradorasPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad ListaAseguradorasPage');
        var id = 'seguros_carlitos';
        this.pouch.getDocumentByID(id).then(function (res) {
            res.aseguradoras.sort(function (a, b) {
                return a.orden - b.orden;
            });
            aseguradoras = res.aseguradoras;
            console.log(aseguradoras);
            _this.aseguradoras = aseguradoras;
            for (var i = 0; i < res.aseguradoras.length; i++) {
                _this.setPic(id, res.aseguradoras[i].logo, i);
            }
            _this.setFilteredItems();
        });
    };
    ListaAseguradorasPage.prototype.visitWeb = function (web) {
        var browser = this.iab.create(web, '_blank', 'location=true,hardwareback=true');
    };
    ListaAseguradorasPage.prototype.callGrua = function (number) {
        console.log("Llamando a grua", number);
        this.callNumber.callNumber(number, true).then(function (res) {
            console.log('calling');
        });
    };
    ListaAseguradorasPage.prototype.setFilteredItems = function () {
        this.aseguradoras = this.filterItems(this.searchTerm);
    };
    ListaAseguradorasPage.prototype.filterItems = function (searchTerm) {
        return aseguradoras.filter(function (item) {
            return item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    ListaAseguradorasPage.prototype.shareWhatsaps = function (item) {
        var msj = 'Te paso los datos de la compañia ' + item.nombre + '. El numero de grua de dicha compañia es: ' + item.grua + '. Por ultimo si queres visitar su sitio web. Te comparto el link: ' + item.web;
        //shareViaWhatsApp(message, image, url)
        this.socialSharing.shareViaWhatsApp(msj, null, null).then(function () {
            // Success!
            alert('La aseguradora se compartio correctamente.');
        }).catch(function () {
            // Error!
            alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.');
        });
    };
    ListaAseguradorasPage.prototype.shareViaMail = function (item) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Enviar via Mail',
            message: "¿A quien desea enviarle los datos de la compañia seleccionada?",
            cssClass: 'contactoAlert',
            inputs: [
                {
                    name: 'email',
                    type: 'email',
                    placeholder: 'Ingese aqui el email del destinatario.'
                },
            ],
            buttons: [
                {
                    text: 'Enviar',
                    handler: function (data) {
                        // Share via email
                        var msj = 'Hago envio de los datos de la compañia ' + item.nombre + '. El numero de grua de dicha compañia es: ' + item.grua + '. Por ultimo si queres visitar su sitio web. Te comparto el link: ' + item.web;
                        _this.socialSharing.shareViaEmail(msj, 'Aseguradora Compartida', data.email).then(function () {
                            // Success!
                            alert('El mensaje se envio correctamente.');
                        }).catch(function () {
                            // Error!
                            alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.');
                        });
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    ListaAseguradorasPage.prototype.setPic = function (id, name, i) {
        var _this = this;
        console.log(id, name, i);
        this.pouch.getImage(id, name).then(function (url) {
            _this.aseguradoras[i].logo = _this._sanitizer.bypassSecurityTrustUrl(url);
        });
    };
    return ListaAseguradorasPage;
}());
ListaAseguradorasPage = __decorate([
    IonicPage({
        name: 'ASEGURADORAS',
        segment: 'lista-aseguradoras'
    }),
    Component({
        selector: 'page-lista-aseguradoras',
        templateUrl: 'lista-aseguradoras.html',
    }),
    __metadata("design:paramtypes", [DomSanitizer, Service, CallNumber, SocialSharing, InAppBrowser, AlertController, NavController, NavParams])
], ListaAseguradorasPage);
export { ListaAseguradorasPage };
//# sourceMappingURL=lista-aseguradoras.js.map