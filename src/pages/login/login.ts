import { Component } from '@angular/core';
import { NavController, AlertController ,Platform,NavParams} from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Service } from '../../providers/service/service';
import { Md5 } from 'ts-md5/dist/md5';
import { MisPolizasPage } from '../mis-polizas/mis-polizas';
import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';

import { LoadingController } from 'ionic-angular';


const url_login: string = 'http://www.meemba.com.ar:5984/seguros_app_02/_design/app/_view/usuarios?limit=1&reduce=false&key=';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  registerCredentials: any = {email: '', password:''};

  constructor(public platform: Platform, public loadingCtrl: LoadingController,public alertCtrl:AlertController ,public pouch: Service, public http: Http, public navCtrl: NavController, public navParams: NavParams,public network: Network) {
     this.platform.registerBackButtonAction(() => {
        this.backHome();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  backHome(){
    this.navCtrl.push(HomePage,{});
  }

  login(user){
  	console.log(user)
    //destroy
    //relogin
      let md5Pass = Md5.hashStr(user.password)
      let url = url_login + '["' + user.email + '","' + md5Pass + '"]';
      let headers = new Headers();
      
          headers.append("Authorization", "Basic " + btoa("seguros" + ":" + "s3gur0s")); 
          //headers.append("Content-Type", "application/x-www-form-urlencoded");
           
          console.log(this.network.type);
         if (this.network.type == 'none') {
              let idLocal = localStorage.getItem('id_user')
              console.log(idLocal)
             this.pouch.getDocumentByID(idLocal).then((doc:any)=>{

               if (doc.pass == md5Pass) {
                 localStorage.setItem('email', user.email);
                 this.navCtrl.push(MisPolizasPage,{ id: doc._id });
  
               }else{
                     this.presentAlert({
                              title: 'INICIO DE SESION ( Sin Internet )', 
                              template: 'Los datos ingresados no coinciden con su perfil guardado. Reintente nuevamente con una conexiona a internet estable.'
                            })
                          
                  }
               
             })

         }else{

           

           this.http.get(url, {headers: headers})
              .map(res => res.json())
              .subscribe(data => {
                  console.log(data);
                  if (data.rows.length > 0) {
                    console.log('login-ok')
                    
                    this.presentLoading();

                    this.pouch.applyFrom(data.rows[0].value, true).then((res: any)=>{
                      console.log('SETEO BD POR USUARIO');
                      console.log(res);
                      localStorage.setItem('email', user.email);
                      localStorage.setItem('id_user',data.rows[0].value);

                      this.navCtrl.push(MisPolizasPage,{ id: data.rows[0].value });
                    });

                  }else{
                     this.presentAlert({
                              title: 'INICIO DE SESION', 
                              template: 'Usuario y/o contraseña incorrectos.'
                            })
                          
                  }
              })
         }


  }

  registrarse(){
   let prompt = this.alertCtrl.create({
              title: 'NUEVO USUARIO',
              // message: "Completa los datos para darte de alta en nuestro sistema.",
              cssClass: 'contactoAlert createAlert',
              inputs: [
                {
                  name: 'dni',
                  type: 'text',
                  placeholder: 'DNI'
                },
                {
                  name: 'nombre',
                  id: 'nombre',
                  type: 'text',
                  placeholder: 'Nombre completo'
                },
                {
                  name: 'email',
                  type: 'email',
                  placeholder: 'Email'
                },
                {
                  name: 'pass',
                  type: 'password',
                  placeholder: 'Contraseña'
                },              ],

              buttons: [
                {
                  text: 'CREAR USUARIO',
                  role: 'ok',
                  handler: data => {
                        // Share via email
                        event.preventDefault();

                       if(!data.pass || !data.email || !data.dni || !data.nombre ){
                            this.presentAlert({
                              title: 'Error en formulario', 
                              template: 'Los campos deben estar completados de forma correcta.'
                            })
                        }else{
                           let md5Pass = Md5.hashStr(data.pass)
                           let ts = new Date().getTime();
                           let empresa = sessionStorage.getItem('empresa')
                           let doc = {
                             '_id': 'usuario_'+ts,
                             'type': 'usuario',
                             'telefono':'',
                             "cliente_de": [empresa],
                             "polizas": [],
                             'dni': data.dni,
                             'nombre': data.nombre,
                             'email': data.email,
                             'clave': md5Pass,
                             'debe_cambiar': false
                           }

                          this.pouch.putDocument(doc).then((res) => {
                            this.presentAlert({
                              title: 'Creacion exitosa', 
                              template: 'Aguarde el recibo del email de confirmacion para poder loguearse.'})
                          })

                        }
                  }
                },
                 {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            });
            prompt.present();
  
  }

  openRestore(){
   let promptContrasena = this.alertCtrl.create({
              title: 'RECORDAR CONTRASEÑA',
              message: "Ingrese sus datos para recordar su contraseña",
              cssClass: 'contactoAlert',
              inputs: [
                {
                  name: 'email',
                  type: 'email',
                  placeholder: 'Ingese aqui su mail.'
                }             
                ],
              buttons: [
                {
                  text: 'Enviar',
                  role: 'ok',
                  handler: data => {
                        // Share via email
                        event.preventDefault();
                        if(data.email){
                           let ts = new Date().getTime();
                           let empresa = sessionStorage.getItem('empresa')
                           let doc = {
                             '_id': 'recordar_'+ts,
                             'type': 'recordar_pass',
                             'empresa': empresa,
                             'email': data.email
                           }

                          this.pouch.putDocument(doc).then((res) => {
                              this.presentAlert({
                              title: 'ENVIADO', 
                              template: 'Aguarde el recibo del email de confirmacion para poder loguearse.'
                              })
                          })
                        }
                  }
                },
                 {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            });
            promptContrasena.present();
  
  }
  
  presentLoading() {
  const loading = this.loadingCtrl.create({
    spinner: 'bubbles',
    content: 'Iniciando Sesión...',
    dismissOnPageChange: true
  });

  loading.present();

}


  presentAlert(obj) {
  let alert = this.alertCtrl.create({
    title: obj.title,
    message: obj.template
  });
  alert.present();

  setTimeout(()=>{
  	alert.dismiss()
  }, 2000)
}


}
