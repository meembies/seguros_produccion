var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Service } from '../../providers/service/service';
import { Md5 } from 'ts-md5/dist/md5';
import { MisPolizasPage } from '../mis-polizas/mis-polizas';
var url_login = 'http://www.meemba.com.ar:5984/seguros_app_02/_design/app/_view/usuarios?limit=1&reduce=false&key=';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(alertCtrl, pouch, http, navCtrl, navParams) {
        this.alertCtrl = alertCtrl;
        this.pouch = pouch;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.registerCredentials = { email: '', password: '' };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function (user) {
        var _this = this;
        console.log(user);
        //destroy
        //relogin
        var md5Pass = Md5.hashStr(user.password);
        console.log(md5Pass);
        var url = url_login + '["' + user.email + '","' + md5Pass + '"]';
        var headers = new Headers();
        headers.append("Authorization", "Basic " + btoa("seguros" + ":" + "s3gur0s"));
        //headers.append("Content-Type", "application/x-www-form-urlencoded");
        this.http.get(url, { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            if (data.rows.length > 0) {
                console.log('login-ok');
                _this.pouch.applyFrom(user.email).then(function (res) {
                    console.log('SETEO BD POR USUARIO');
                    localStorage.setItem('email', user.email);
                    _this.navCtrl.push(MisPolizasPage, { id: user.email });
                });
            }
            else {
                alert('Usuario y/o Contraseña incorrecto.');
            }
        });
    };
    LoginPage.prototype.registrarse = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'NUEVO USUARIO',
            message: "Completa los datos para darte de alta en nuestro sistema.",
            cssClass: 'contactoAlert',
            inputs: [
                {
                    name: 'email',
                    type: 'email',
                    placeholder: 'Ingese aqui su mail.'
                },
                {
                    name: 'pass_1',
                    id: 'pass_1',
                    type: 'password',
                    placeholder: 'Ingese aqui su contraseña.'
                },
                {
                    name: 'pass_2',
                    type: 'password',
                    placeholder: 'Repetir su contraseña.'
                },
            ],
            buttons: [
                {
                    text: 'Enviar',
                    role: 'ok',
                    handler: function (data) {
                        // Share via email
                        event.preventDefault();
                        if (data.pass_1 != data.pass_2) {
                            alert('Las contraseñas no coinciden.');
                        }
                        else {
                            Md5pass_1;
                            var doc = {
                                'email': data.email,
                                'pass': 
                            };
                            _this.pouch.putDocument(doc).then(function (res) {
                            });
                        }
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.openRestore = function () {
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Component({
        selector: 'page-login',
        templateUrl: 'login.html',
    }),
    __metadata("design:paramtypes", [AlertController, Service, Http, NavController, NavParams])
], LoginPage);
export { LoginPage };
//# sourceMappingURL=login.js.map