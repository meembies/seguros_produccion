import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
// import { DatePipe } from '@angular/common';

import { GenerarSiniestroPage } from '../generar-siniestro/generar-siniestro';
import { Service } from '../../providers/service/service';
import { HomePage } from '../home/home';

/**
 * Generated class for the ListaSiniestrosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
// @IonicPage({
//   name: 'MIS SINIESTROS',
//   segment: 'ListaSiniestrosPage'
// })
@Component({
  selector: 'page-lista-siniestros',
  templateUrl: 'lista-siniestros.html',
})
export class ListaSiniestrosPage {
  siniestros:any;
  constructor(public platform: Platform, public service: Service, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.platform.registerBackButtonAction(() => {
        this.backHome();
    })
  }

  backHome(){
    this.navCtrl.push(HomePage,{});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaSiniestrosPage');
   
  }
  
  ionViewDidEnter() {
     
     this.getAllSiniestros()

  }

  crearSiniestro(item){
     this.navCtrl.push(GenerarSiniestroPage,{item:item});
  }

  getAllSiniestros(){
     this.service.getSiniestros().then((res)=>{
       this.siniestros = res
     });
  }

  onHold(s,i) {
    console.log(s)
          let alertSiniestro = this.alertCtrl.create({
              title: 'SINIESTRO 23-02-2017',
              cssClass: 'contactoAlert',
              message: 'A continuacion se detallan las diferentes acciones que puede realizar sobre el siniestro que ha generado.',
              buttons: [

              {
                  text: 'VER',
                  role: 'ok',
                  handler: () => {
                            this.navCtrl.push(GenerarSiniestroPage,{item:s});

                    console.log('Buy clicked');
                  }
              },

              {
                  text: 'ELIMINAR',
                  role: 'ok',
                  handler: () => {
                     
                     this.service.siniestros.get(s._id).then((doc)=>{
                          doc._deleted = true;
                       this.service.siniestros.put(doc).then((res)=>{
                           console.log(res)
                           this.siniestros.splice(i, 1);
                       });

                     })
                     console.log('Siniestro Eliminado.')
                  }
              },


                {
                  text: 'CANCELAR',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
                
              ]
            });
          
          alertSiniestro.present();
  }


  verDetalle(){

  }
}
