var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
// import { DatePipe } from '@angular/common';
import { GenerarSiniestroPage } from '../generar-siniestro/generar-siniestro';
import { Service } from '../../providers/service/service';
/**
 * Generated class for the ListaSiniestrosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ListaSiniestrosPage = (function () {
    function ListaSiniestrosPage(service, alertCtrl, navCtrl, navParams) {
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ListaSiniestrosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListaSiniestrosPage');
    };
    ListaSiniestrosPage.prototype.ionViewDidEnter = function () {
        this.getAllSiniestros();
    };
    ListaSiniestrosPage.prototype.crearSiniestro = function (item) {
        this.navCtrl.push(GenerarSiniestroPage, { item: item });
    };
    ListaSiniestrosPage.prototype.getAllSiniestros = function () {
        var _this = this;
        this.service.getSiniestros().then(function (res) {
            _this.siniestros = res;
        });
    };
    ListaSiniestrosPage.prototype.onHold = function (s, i) {
        var _this = this;
        console.log(s);
        var alertSiniestro = this.alertCtrl.create({
            title: 'SINIESTRO 23-02-2017',
            cssClass: 'contactoAlert',
            message: 'A continuacion se detallan las diferentes acciones que puede realizar sobre el siniestro que ha generado.',
            buttons: [
                {
                    text: 'VER',
                    role: 'ok',
                    handler: function () {
                        _this.navCtrl.push(GenerarSiniestroPage, { item: s });
                        console.log('Buy clicked');
                    }
                },
                {
                    text: 'ELIMINAR',
                    role: 'ok',
                    handler: function () {
                        _this.service.siniestros.get(s._id).then(function (doc) {
                            doc._deleted = true;
                            _this.service.siniestros.put(doc).then(function (res) {
                                console.log(res);
                                _this.siniestros.splice(i, 1);
                            });
                        });
                        console.log('Siniestro Eliminado.');
                    }
                },
                {
                    text: 'CANCELAR',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alertSiniestro.present();
    };
    ListaSiniestrosPage.prototype.verDetalle = function () {
    };
    return ListaSiniestrosPage;
}());
ListaSiniestrosPage = __decorate([
    IonicPage({
        name: 'MIS SINIESTROS',
        segment: 'ListaSiniestrosPage'
    }),
    Component({
        selector: 'page-lista-siniestros',
        templateUrl: 'lista-siniestros.html',
    }),
    __metadata("design:paramtypes", [Service, AlertController, NavController, NavParams])
], ListaSiniestrosPage);
export { ListaSiniestrosPage };
//# sourceMappingURL=lista-siniestros.js.map