var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { GenerarCotizacionPage } from '../generar-cotizacion/generar-cotizacion';
import { ListaAseguradorasPage } from '../lista-aseguradoras/lista-aseguradoras';
import { ListaSiniestrosPage } from '../lista-siniestros/lista-siniestros';
import { MisPolizasPage } from '../mis-polizas/mis-polizas';
import { LoginPage } from '../login/login';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';
import { Service } from '../../providers/service/service';
// declare var window;
var HomePage = (function () {
    function HomePage(platform, _sanitizer, pouch, socialsharing, callNumber, alertCtrl, navCtrl) {
        this.platform = platform;
        this._sanitizer = _sanitizer;
        this.pouch = pouch;
        this.socialsharing = socialsharing;
        this.callNumber = callNumber;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.empresa = { id: '', email: '', telefono: '', atencion: '' };
        this.banners = ['./assets/images/sin-imagen.png'];
    }
    HomePage.prototype.goToSlide = function () {
        this.slides.slideTo(1, 500);
    };
    HomePage.prototype.ionViewCanEnter = function () {
        console.log("corroboro-bd");
        this.bdCreada = false;
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log("didload");
        // this.pouch.dataBase.info().then( (result)  => {
        //                       // handle result
        //                       console.log(result);
        //                     }).catch( (err) => {
        //                       console.log(err);
        //                     });
    };
    HomePage.prototype.ionViewWillEnter = function () {
        console.log("willenter");
        this.banners = [];
        this.getBanners();
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var d = new Date();
        this.dia = d.getDay();
        this.hora = d.getHours();
    };
    HomePage.prototype.getBanners = function () {
        var _this = this;
        var id = 'seguros_carlitos';
        sessionStorage.setItem('empresa', id);
        this.pouch.getDocumentByID(id).then(function (res) {
            _this.empresa.email = res.email;
            _this.empresa.telefono = res.telefono;
            _this.empresa.atencion = res.atencion;
            if (res.banners.length > 1) {
                res.banners.sort(function (a, b) {
                    return a.orden - b.orden;
                });
            }
            for (var _i = 0, _a = res.banners; _i < _a.length; _i++) {
                var banner = _a[_i];
                _this.pouch.getImage(id, banner.img).then(function (url) {
                    if (_this.banners[0] == './assets/images/sin-imagen.png') {
                        console.log('vacio-banner-array');
                        _this.banners = [];
                    }
                    _this.banners.push(_this._sanitizer.bypassSecurityTrustUrl(url));
                });
            }
            _this.aseguradoras = res.aseguradoras;
            console.log(_this.banners);
        });
    };
    HomePage.prototype.goToAseguradoras = function () {
        if (this.aseguradoras) {
            this.navCtrl.push(ListaAseguradorasPage, { aseguradoras: this.aseguradoras });
        }
        else {
            this.navCtrl.push(ListaAseguradorasPage, { aseguradoras: null });
        }
    };
    HomePage.prototype.goToDesktop = function () {
        this.platform.exitApp();
    };
    HomePage.prototype.goToSiniestros = function () {
        this.navCtrl.push(ListaSiniestrosPage, {});
    };
    HomePage.prototype.goToCotizar = function () {
        this.navCtrl.push(GenerarCotizacionPage, {});
    };
    HomePage.prototype.goToPolizas = function () {
        var email = localStorage.getItem('email');
        var email_def = localStorage.getItem('default');
        if (email != email_def) {
            this.navCtrl.push(MisPolizasPage, {});
        }
        else {
            this.navCtrl.push(LoginPage, {});
        }
    };
    HomePage.prototype.goToContacto = function () {
        if (this.empresa.atencion[this.dia]) {
            if (this.hora >= this.empresa.atencion[this.dia].inicio && this.hora < this.empresa.atencion[this.dia].fin) {
                this.contactNormal(this.empresa.telefono);
            }
            else {
                this.contactNocturno();
            }
        }
        else {
            this.contactNocturno();
        }
    };
    HomePage.prototype.contactNocturno = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'CONSULTA',
            message: "¿Cual es su consulta?",
            cssClass: 'contactoAlert',
            inputs: [
                {
                    name: 'email',
                    type: 'email',
                    placeholder: 'Ingese aqui su mail.'
                },
                {
                    name: 'consulta',
                    type: 'text',
                    placeholder: 'Ingese aqui su consulta.'
                },
            ],
            buttons: [
                {
                    text: 'Enviar',
                    role: 'ok',
                    handler: function (data) {
                        // Share via email
                        var msj = 'mail: ' + data.email + ' Consulta:' + data.consulta;
                        _this.socialsharing.shareViaEmail(msj, 'Consulta Desde App', ['web@meemba.com.ar', _this.empresa.mail]).then(function () {
                            // Success!
                            alert('El mensaje se envio correctamente.');
                        }).catch(function () {
                            // Error!
                            alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.');
                        });
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage.prototype.contactNormal = function (number) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'CONTACTO',
            subTitle: '¿ Esta seguro que desea comunicarse telefonicamente ?',
            message: 'Esta llamada puede ocasionar costos extras con su operador.',
            cssClass: 'contactoAlert',
            buttons: [
                {
                    text: 'LLAMAR',
                    handler: function () {
                        console.log('Llamar');
                        _this.callNumber.callNumber(number, true);
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                        console.log('Cancelar');
                    }
                }
            ]
        });
        confirm.present();
    };
    return HomePage;
}());
__decorate([
    ViewChild(Slides),
    __metadata("design:type", Slides)
], HomePage.prototype, "slides", void 0);
HomePage = __decorate([
    Component({
        selector: 'page-home',
        templateUrl: 'home.html'
    }),
    __metadata("design:paramtypes", [Platform, DomSanitizer, Service, SocialSharing, CallNumber, AlertController, NavController])
], HomePage);
export { HomePage };
//# sourceMappingURL=home.js.map