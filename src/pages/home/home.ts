import { Component } from '@angular/core';
import { NavController,AlertController ,Platform } from 'ionic-angular';


import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

import { GenerarCotizacionPage } from '../generar-cotizacion/generar-cotizacion';
import { ListaAseguradorasPage } from '../lista-aseguradoras/lista-aseguradoras';
import { ListaSiniestrosPage } from '../lista-siniestros/lista-siniestros';
import { MisPolizasPage } from '../mis-polizas/mis-polizas';
import { LoginPage } from '../login/login';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AndroidPermissions } from '@ionic-native/android-permissions';


import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';

import { Service } from '../../providers/service/service';
import { OneSignal } from '@ionic-native/onesignal';

// declare var window;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Slides) slides: Slides;
  aseguradoras: any;
  empresa: any = { nombre: 'Seguros Atender', id: '', email : '', telefono: '', atencion: '' };
  banners:any = ['./assets/images/sin-imagen.png'];
  hora: any;
  dia: any;
  constructor(
            private androidPermissions: AndroidPermissions, 
            private oneSignal: OneSignal, 
            private iab: InAppBrowser, 
            public platform: Platform, 
            private _sanitizer: DomSanitizer, 
            private pouch: Service, 
            private socialsharing: SocialSharing, 
            private callNumber: CallNumber, 
            public alertCtrl:AlertController, 
            public navCtrl: NavController) {
     
    let email = localStorage.getItem('email');

    if (email) {
    this.oneSignal.sendTags({
          email: localStorage.getItem('email'),
          empresa: 'atender'
        });
    console.log("setTags")
    }

    this.platform.registerBackButtonAction(() => {
        this.goToDesktop();
    })

  }

  goToSlide() {
    this.slides.slideTo(1, 500);
  }

  ionViewCanEnter(){
  	console.log("corroboro-bd")

  }

  ionViewDidLoad() { 
    console.log("didload");  
    let bdCreada = localStorage.getItem('db')
    console.log(bdCreada)
    
    if (bdCreada) {
           this.banners = [];
           this.getBanners();
    }  
    
  }

  ionViewWillEnter(){
    let bdCreada = localStorage.getItem('db')
    console.log(bdCreada)
    
    if (!bdCreada) {
         setTimeout(()=>{
           this.banners = [];
           this.getBanners();
      }, 1000)
    }

  }

  ionViewDidEnter(){
    var d = new Date();
    this.dia = d.getDay();
    this.hora = d.getHours();
    
  }

 
  getBanners(){
    let status = false;

    let id = 'seguros_atender'
    sessionStorage.setItem('empresa',id);


      this.pouch.getDocumentByID(id).then((res: any)=>{
       console.log(res);
       this.empresa.web = res.web;
       this.empresa.email= res.email;
       this.empresa.telefono= res.telefono;
       this.empresa.whatsapp= res.whatsapp;
       this.empresa.atencion= res.atencion;
       this.empresa.nombre = res.slogan;
       this.empresa.msj_contacto = res.msj_contacto;

       if (res.banners.length > 1) {

       res.banners.sort((a, b) =>{
         return a.orden - b.orden
       });

      

     }

     for(let banner of res.banners){
     
       this.pouch.getImage(id, banner.img).then((url:any)=>{
          if(this.banners[0] == './assets/images/sin-imagen.png'){
            console.log('vacio-banner-array')
            this.banners = [];
           
         }
         this.banners.push(this._sanitizer.bypassSecurityTrustUrl(url))  
       })
      }
     
       this.aseguradoras = res.aseguradoras;
       console.log(this.banners)
     });

     if (this.banners.length > 0) {
       status = true;
     }

    if(!status){
        setTimeout(()=>{
        this.getBanners();
      }, 2000)
    }
  }

  goToAseguradoras() {
    if (this.aseguradoras) {
      this.navCtrl.push(ListaAseguradorasPage,{ aseguradoras:this.aseguradoras });
    }else{
       this.navCtrl.push(ListaAseguradorasPage,{ aseguradoras: null });
    }  
  }


  goToDesktop() {
     this.platform.exitApp();
  }
  
  goToSiniestros() {
    this.navCtrl.push(ListaSiniestrosPage,{});
  }

  goToCotizar() {
    this.navCtrl.push(GenerarCotizacionPage,{});
  }

  goToPolizas() {
    let email = localStorage.getItem('email');
    let email_def = localStorage.getItem('default');

    if (email != email_def) {
      this.navCtrl.push(MisPolizasPage,{});
    }else{
      this.navCtrl.push(LoginPage,{});
    }
  }



  goToContacto() {

    if (this.empresa.atencion[this.dia]) {


       let inicio = parseFloat(this.empresa.atencion[this.dia].inicio)
       console.log(parseFloat(this.hora))
       let fin = parseFloat(this.empresa.atencion[this.dia].fin)
       
      if (this.hora >= inicio && this.hora < fin) {
          this.contactNormal(this.empresa.telefono)
      }else{
          this.contactNormal(this.empresa.telefono)
      }
   
    }else{
          this.contactNormal(this.empresa.telefono)
    }



  }

    contactNocturno(){
     let prompt = this.alertCtrl.create({
                title: 'CONSULTA',
                message: "¿Cual es su consulta?",
                cssClass: 'contactoAlert',
                inputs: [
                  {
                    name: 'email',
                    type: 'email',
                    placeholder: 'Email.'
                  },
                  {
                    name: 'consulta',
                    type: 'text',
                    placeholder: 'Consulta.'
                  },
                ],

                buttons: [
                  {
                    text: 'Enviar',
                    role: 'ok',
                    handler: data => {
                          // Share via email
                          var msj: string = 'mail: '+data.email+' Consulta:' +data.consulta;
                          this.socialsharing.shareViaEmail(msj, 'Consulta Desde App', [this.empresa.mail]).then(() => {
                            // Success!
                            console.log('El mensaje se envio correctamente.');

                          }).catch(() => {
                            // Error!
                            alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.')

                          });

                    }
                  },
                   {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              prompt.present();
    
    }

  contactNormal(number){
  
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CALL_PHONE).then(
  (result) =>{

    let inicio = parseFloat(this.empresa.atencion[this.dia].inicio)
    console.log(parseFloat(this.hora))
    let fin = parseFloat(this.empresa.atencion[this.dia].fin)
    
    let msj = this.empresa.nombre+' en forma personal atiende de Lunes a Viernes de 9 a 18hs.'

    if (this.empresa.msj_contacto) {
      msj = this.empresa.msj_contacto
    }
    let confirm = this.alertCtrl.create({
        title: 'CONTACTO',
        subTitle: 'Seleccione una forma para contactarse.',
        message: msj,
        cssClass: 'contactoAlert',
        buttons: [
          {
            text: 'WHATSAPP',
            handler: () => {
              console.log('LlamarWhatsapp');
              if (this.empresa.whatsapp) {
                let receiver = this.empresa.whatsapp;
                 
                 this.socialsharing.shareViaWhatsAppToReceiver(receiver, 'Ha recibido una nueva consulta desde su aplicación.').then(() => {
                   console.log('exito')
                 }).catch(() => {
                          // Error!
                          alert('Parece que tu dispositivo no es compatible con esta funcion. En su lugar, te contactaremos por telefono para un trato mas directo.');
                          this.callNumber.callNumber(number, false);
                          })  
              }else{
                alert('Aun no poseemos una atencion via whatsapp. En su lugar, preferimos que nos contactes por telefono para un trato mas directo.');
                this.callNumber.callNumber(number, false);
              }
              
              }
          },
          {
            text: 'LLAMAR',
            handler: () => {
              console.log('Llamar');
              if (number) {
                this.callNumber.callNumber(number, false);
              }else{
                alert('Aun no se posee un numero cargado para realizar la llamada.')
              }
            }
          },
          {
            text: 'ENVIAR EMAIL',
            handler: () => {
              console.log('ENVIAR EMAIL');

             if (this.empresa.email) {
               this.socialsharing.shareViaEmail('', 'Consulta Desde App', [this.empresa.email,'']).then(() => {
                            // Success!
                            alert('El mensaje se envio correctamente.');

                          }).catch(() => {
                            // Error!
                            alert('Hubo un error intentando enviar el mensaje, aguarde y reintente la accion.')

                          });
              }else{
                alert('Aun no se posee un email cargado para realizar la consulta.')
              }          

            }
          },
           {
            text: 'VISITAR SITIO WEB',
            handler: () => {
              let web = this.empresa.web;
              console.log('SITIO WEB',web);
              if (web) {
                  let browser = this.iab.create(web,'_blank','location=true,hardwareback=true');
              }  
            }
          },
          {
            text: 'Cancelar',
            handler: () => {
              console.log('Cancelar');
            }
          }
        ]
      });
      confirm.present();

    },(err) =>{ 
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE)
      this.contactNormal(number);
    });

  }

  visitarWeb(web){

    if (!web) {
       web = this.empresa.web;
    }
    console.log('SITIO WEB-banner',web);
            let browser = this.iab.create(web,'_blank','location=true,hardwareback=true');
  }

  // changeUser(){

  //   this.pouch.replicate.cancel()
    
  //    let opcionesFrom = {
  //             live:true,
  //             retry:true,
  //             continuos:true,
  //             doc_ids:['web@meemba.com.ar', 'seguros_caeiro'],
  //             filter: '_view',
  //             view: 'app/datos',
  //             auth: {
  //               username: 'seguros',
  //               password: 's3gur0s'
  //             }
  //           }

  //  this.pouch.dataBase.replicate.from('http://www.meemba.com.ar:5984/seguros_app_02', opcionesFrom).on('change', function (info) {
  //               console.log("changeFrom DB")
  //             }).on('paused', function (info) {
  //               console.log("pausedFrom  DB")
  //             }).on('active', function (info) {
  //               console.log("activedFrom  DB")
  //             }).on('complete', function(info){
  //               console.log('complete');
  //               localStorage.setItem('secondLoad','OK')  
  //             })

  // }

  // getAlldocs(){
  //   this.pouch.getDocuments().then((res)=>{
  //     console.log('newDoc')
  //     console.log(res);
  //   })
  // }

}
