import { Component } from '@angular/core';
import { Platform, NavController ,AlertController , NavParams } from 'ionic-angular';

import { Service } from '../../providers/service/service';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import {Md5} from 'ts-md5/dist/md5';
import { OneSignal } from '@ionic-native/onesignal';
import { HomePage } from '../home/home';
import { SocialSharing } from '@ionic-native/social-sharing';

// declare var window;  
const options: any = {
  title: 'Poliza',
  openWith: { 'enabled ': true }
}

/**
 * Generated class for the MisPolizasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-mis-polizas',
  templateUrl: 'mis-polizas.html',
})
export class MisPolizasPage {
  polizas:any =[ { nombre: 'No posee polizas asignadas.' ,  tipo: 'NUEVO USUARIO',numero: '000000', ocultar:true }];
  
  constructor(private fileOpener: FileOpener, private socialsharing: SocialSharing, public platform: Platform,private oneSignal: OneSignal, public alertCtrl:AlertController , public file: File, public document: DocumentViewer, public pouch: Service, public navCtrl: NavController, public navParams: NavParams) {
  
    this.platform.registerBackButtonAction(() => {
        this.backHome();
    })

  }  

  ionViewDidEnter() {
  
  
 	let id = this.navParams.get('id');
	
	if (!id) {
		id = localStorage.getItem('id_user');
	}

  console.log(id);

	  	this.pouch.getDocumentByID(id).then((res: any)=>{
	  		console.log(res);	
        
        this.oneSignal.sendTags({
          email: res.email.toLowerCase(),
          empresa: 'atender'
        });

        if (res.debe_cambiar) {
          console.log('cambiar_pass');
          this.changePass(res)  
        }

	  		if (res.polizas) {
	  			if (res.polizas.length > 0) {
	  				this.polizas = res.polizas;
	  			}else{
	  				console.log('sin-polizas')
	  			}
	  		}
	  	})

    console.log('ionViewDidLoad MisPolizasPage');

    setTimeout(()=>{
      if (this.polizas[0].numero == '000000' || this.polizas[0].numero == null) {
        console.log('Polizas en Proceso');
        this.reSincPolizas()
      }

    },4000)
  }
  
  changePass(oldDoc){
    
    let prompt = this.alertCtrl.create({
             title: 'CAMBIAR CONTRASEÑA',
              message: "Al parecer debes cambiar tu contraseña.",
              cssClass: 'contactoAlert',
              inputs: [
                {
                  name: 'pass',
                  type: 'password',
                  placeholder: 'Contraseña'
                }
              ],

              buttons: [
                {
                  text: 'Enviar',
                  role: 'ok',
                  handler: data => {
                        // Share via email
                        event.preventDefault();

                       if(!data.pass){
                            alert('Los campos deben estar completados de forma correcta.')
                        }else{
                           let md5Pass = Md5.hashStr(data.pass)
                            
                           let doc = oldDoc;

                           doc.clave = md5Pass;
                           doc.debe_cambiar = false;
                            
                          this.pouch.putDocument(doc).then((res) => {
                            alert('Cambio de contraseña exitoso.')
                          })
                       } 
                  }
                },
                 {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            });
            prompt.present();
 
  }



  
  viewDocument(archivo){
  	console.log('view')

      let id = this.navParams.get('id');
      // let ts = new Date().getTime();
      
      if (!id) {
        id = localStorage.getItem('id_user');
      }
      console.log(id)
   
    this.pouch.getAttachmentPdf(id, archivo).then((blob: any)=>{
      let ts = new Date().getTime();
      let name = 'poliza_'+ts+'.pdf';
      
      this.file.createDir(this.file.dataDirectory,'MisPolizas',true).then((dir:any)=>{
        console.log(dir);
         this.file.writeFile(dir.nativeURL, name, blob).then((res)=>{
           console.log(res.nativeURL)
           let url = res.nativeURL;
           
           if(this.platform.is('ios')){
             this.viewDoc(url);
           }else{
             this.openPDF(url);
           }


         },(err)=>{
           alert(err)
         })
      })




    })
  }

  viewDoc(url){
        function onPossible() {
           document.addEventListener('deviceready', function () {
                // cordova.plugins.SitewaertsDocumentViewer is now available

            }, false);
           }
           
           function onError(error) {
             console.log(error);
            alert("Sorry! Cannot show document.");

           }

           function onImpossible(argument) {
             console.log(argument)  
           }

           function onMissingApp(appId, installer) {
             /*com.google.android.apps.pdfviewer*/
            console.log('El Documento no puede ser mostrado.');
            alert('Necesita un lector de pdf compatible.')
            installer();
           }

          this.document.canViewDocument(url, 'application/pdf', options, onPossible, onMissingApp, onImpossible, onError);
          
          this.document.viewDocument(url, 'application/pdf', options)

  }

  openPDF(url){

    this.fileOpener.open(url, 'application/pdf')
      .then(() => {
        console.log('File is opened')

      })
      .catch(e => {
        console.log('Error openening file', e)
        this.viewDoc(url);
      });

  }
  
  downloadDocument(archivo){
    console.log('download')

      let id = this.navParams.get('id');
      
      if (!id) {
        id = localStorage.getItem('id_user');
      }
  
      console.log(id,archivo);

        this.pouch.getAttachmentPdf(id, archivo).then((blob: any)=>{
          let ts = new Date().getTime();
          let name = 'poliza_'+ts+'.pdf';

          if(this.platform.is('ios')){
            // ios...
            console.log('DESCARNDO-IOS');

             // this.socialsharing.shareWithOptions({files:blob, chooserTitle: name}).then((res) => {

             //   console.log('res')
             //   console.log(res)
             // })

              this.file.writeFile(this.file.documentsDirectory, name, blob).then((res)=>{
               
               let url = res.nativeURL;
               console.log(url)
               
               this.socialsharing.share('message', 'subject', url, 'url').then((res) => {

               console.log('res')
               console.log(res)
             })
               console.log('La poliza se ha descargado correctamente en su carpeta personal.')

             },(err)=>{
               alert(err)
             })


          }else{
            // android...
          this.file.createDir(this.file.externalRootDirectory,'Download',true).then((dir:any)=>{
            console.log(dir);
             this.file.writeFile(dir.nativeURL, name, blob).then((res)=>{
               let url = res.nativeURL;
              
               let open = this.alertCtrl.create({
                         title: 'DESCARGA EXITOSA',
                          message: "Tu poliza ya se encuentra guardada en tu dispositivo",
                          cssClass: 'contactoAlert',
                          buttons: [
                            {
                              text: 'Abrir ahora',
                              role: 'ok',
                              handler: data => {
                                    event.preventDefault();
                                    this.openPDF(url);
                              }
                            },
                            {
                              text: 'Buscar más tarde',
                              role: 'cancel',
                              handler: () => {
                                alert('La poliza se ha descargado correctamente en su carpeta de descargas habitual.')
                              }
                            },
                            {
                              text: 'Cancelar',
                              role: 'cancel',
                              handler: () => {
                                console.log('Cancel clicked');
                              }
                            }
                          ]
                        });
                        
                        open.present();
             

             },(err)=>{
               alert(err)
             })
            })

          }
      })

  }

  closeSession(){
      let idLocal = localStorage.getItem('id_user')
      sessionStorage.clear()
      localStorage.clear()
      localStorage.setItem('id_user',idLocal)

      this.oneSignal.deleteTags(['empresa','email']);

      this.pouch.applyFrom('default@seguros_carlitos',false).then((res)=>{
                      console.log('SETEO BD POR USUARIO');
                      
                      localStorage.setItem('email','default@seguros_carlitos');
                      localStorage.setItem('default','default@seguros_carlitos');
;
                      this.navCtrl.push(HomePage,{});
                    });


  }

  reSincPolizas(){
      
      let id = localStorage.getItem('id_user');
      console.log(id)
      
      this.pouch.dataBase.get(id).then((res: any)=>{
        console.log(res);  

        this.oneSignal.sendTags({
          email: res.email.toLowerCase(),
          empresa: 'atender'
        });

        if (res.polizas) {
          if (res.polizas.length > 0) {
            this.polizas = res.polizas;
          }else{
            console.log('sin-polizas-aun')
          }
        }
      },(err)=>{
        console.log(err);
      })
  }

  backHome(){
                      this.navCtrl.push(HomePage,{});
  }

}
