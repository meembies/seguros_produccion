var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Service } from '../../providers/service/service';
/**
 * Generated class for the MisPolizasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MisPolizasPage = (function () {
    function MisPolizasPage(pouch, navCtrl, navParams) {
        this.pouch = pouch;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.polizas = [{ nombre: 'Cargando Polizas...', numero: '' }];
    }
    MisPolizasPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var id = this.navParams.get('id');
        if (!id) {
            id = localStorage.getItem('email');
        }
        this.pouch.getDocumentByID(id).then(function (res) {
            console.log(res);
            if (res.polizas) {
                if (res.polizas.length > 0) {
                    _this.polizas = res.polizas;
                }
                else {
                    console.log('sin-polizas');
                }
            }
        });
        console.log('ionViewDidLoad MisPolizasPage');
    };
    MisPolizasPage.prototype.viewDocument = function (archivo) {
        console.log('view');
    };
    MisPolizasPage.prototype.downloadDocument = function (archivo) {
        console.log('download');
    };
    return MisPolizasPage;
}());
MisPolizasPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-mis-polizas',
        templateUrl: 'mis-polizas.html',
    }),
    __metadata("design:paramtypes", [Service, NavController, NavParams])
], MisPolizasPage);
export { MisPolizasPage };
//# sourceMappingURL=mis-polizas.js.map