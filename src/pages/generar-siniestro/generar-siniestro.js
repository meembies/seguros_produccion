var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
//import { HomePage } from '../home/home';
import { Images } from '../../providers/images/images';
import { Service } from '../../providers/service/service';
//import { DomSanitizer } from '@angular/platform-browser';
import { ListaSiniestrosPage } from '../lista-siniestros/lista-siniestros';
// import { Media, MediaObject } from '@ionic-native/media';
// import { File } from '@ionic-native/file';
// declare var cordova: any;
// declare var window: any;
/**
 * Generated class for the GenerarSiniestroPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var GenerarSiniestroPage = (function () {
    function GenerarSiniestroPage(alertCtrl, service, img, navCtrl, navParams) {
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.img = img;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.myDate = new Date().toISOString();
        this.accion = 'GUARDAR SINIESTRO';
        this.modificar = true;
        console.log(this.myDate);
        var empresa = sessionStorage.getItem('empresa');
        this.form = {
            fecha: this.myDate,
            empresa: empresa,
            nombre: '',
            email: '',
            telefono_01: '',
            telefono_02: '',
            lugar: '',
            dominio: '',
            aseguradora: '',
            marca: '',
            modelo: '',
            images: ['./assets/images/sin-imagen.png']
        };
    }
    GenerarSiniestroPage.prototype.ionViewDidLoad = function () {
        console.log(' didload GenerarSiniestroPage');
        var siniestro = this.navParams.get('item');
        console.log(siniestro);
        if (siniestro) {
            this.accion = "MODIFICAR SINIESTRO";
            this.form = siniestro;
        }
        else {
            this.modificar = false;
        }
    };
    GenerarSiniestroPage.prototype.ionViewDidEnter = function () {
        this.fecha = this.myDate;
        console.log(' didenter GenerarSiniestroPage');
        // this.nota = this.media.create('file.mp3');
    };
    GenerarSiniestroPage.prototype.ionViewDidLeave = function () {
        // this.nota.release();
    };
    // saveAudio(){
    //   this.nota.startRecord();
    // }
    // playAudio(){
    //   this.nota.play();
    // }
    // stopAudio(){
    //   this.nota.stopRecord();
    // }
    GenerarSiniestroPage.prototype.Save = function (form) {
        var _this = this;
        var siniestro = this.navParams.get('item');
        console.log(siniestro);
        if (siniestro) {
            this.service.siniestros.put(form).then(function (res) {
                console.log('doc:');
                console.log(res);
                alert('SALVADO');
            });
        }
        else {
            var date = new Date();
            var ts = date.getDate();
            ts = ts + date.getSeconds();
            form._id = 'siniestro_' + ts;
            form.type = 'siniestro';
            form.fecha_2 = form.fecha.split('T');
            for (var i = 0; i < this.form.images.length; i++) {
                if (!this.form.images[i]) {
                    this.form.images[i] = 'null';
                }
            }
            /* si hay imagenes */
            form.favorito = false;
            console.log(form);
            this.service.siniestros.put(form).then(function (res) {
                console.log('doc:');
                console.log(res);
                _this.navCtrl.push(ListaSiniestrosPage);
            });
        }
    };
    ;
    GenerarSiniestroPage.prototype.delete = function (form) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'ELIMINAR SINIESTRO',
            subTitle: '',
            message: '¿Esta seguro que desea eliminar este siniestro?',
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                        console.log("Cancelamos la accion.");
                    }
                },
                {
                    text: 'SI',
                    handler: function () {
                        _this.service.siniestros.get(form._id).then(function (doc) {
                            doc._deleted = true;
                            _this.service.siniestros.put(doc).then(function (res) {
                                console.log(res);
                                _this.navCtrl.push(ListaSiniestrosPage);
                            });
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    GenerarSiniestroPage.prototype.Send = function (form) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'ENVIAR SINIESTRO',
            message: "¿ A quienes deseea enviarle este siniestro ?",
            cssClass: 'contactoAlert',
            inputs: [
                {
                    name: 'email_01',
                    type: 'email',
                    placeholder: 'De:'
                },
                {
                    name: 'email_02',
                    type: 'email',
                    placeholder: 'Para:'
                }
            ],
            buttons: [
                {
                    text: 'Enviar',
                    role: 'ok',
                    handler: function (data) {
                        var doc = form;
                        if (data.email_01 && data.email_02) {
                            var shot = "data:image/jpeg;base64,";
                            doc.de_email = data.email_01;
                            doc.para_email = data.email_02;
                            doc._attachments = {};
                            if (!doc.empresa) {
                                doc.empresa = sessionStorage.getItem('empresa');
                            }
                            //sin-imagenes
                            if (doc.images[0] == './assets/images/sin-imagen.png') {
                                doc.images = null;
                            }
                            else {
                                if (doc.images[0] && doc.images[0].length > 10) {
                                    var foto_01 = doc.images[0].replace(shot, '');
                                    doc._attachments.foto_01 = {
                                        content_type: 'image/jpeg',
                                        data: foto_01
                                    };
                                }
                                if (doc.images[1] && doc.images[1].length > 10) {
                                    var foto_02 = doc.images[1].replace(shot, '');
                                    doc._attachments.foto_02 = {
                                        content_type: 'image/jpeg',
                                        data: foto_02
                                    };
                                }
                                if (doc.images[2] && doc.images[2].length > 10) {
                                    var foto_03 = doc.images[1].replace(shot, '');
                                    doc._attachments.foto_03 = {
                                        content_type: 'image/jpeg',
                                        data: foto_03
                                    };
                                }
                                if (doc.images[3] && doc.images[3].length > 10) {
                                    var foto_04 = doc.images[3].replace(shot, '');
                                    doc._attachments.foto_04 = {
                                        content_type: 'image/jpeg',
                                        data: foto_04
                                    };
                                }
                                if (doc.images[4] && doc.images[4].length > 10) {
                                    var foto_05 = doc.images[4].replace(shot, '');
                                    doc._attachments.foto_05 = {
                                        content_type: 'image/jpeg',
                                        data: foto_05
                                    };
                                }
                                doc.images = null;
                            }
                            var ts = (new Date()).getTime();
                            doc.fecha = ts;
                            doc._id = 'siniestro' + ts;
                            doc._rev = null;
                            _this.service.dataBase.put(doc).then(function (res) {
                                console.log('doc:');
                                console.log(res);
                                alert('ENVIADO');
                            });
                        }
                        else {
                            alert('COMPLETE LOS CAMPOS CORRECTAMENTE.');
                        }
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel send!');
                    }
                }
            ]
        });
        prompt.present();
    };
    GenerarSiniestroPage.prototype.addImageToSlider = function () {
        var _this = this;
        if (this.form.images.length < 5) {
            this.img.presentActionSheet().then(function (base64) {
                if (base64 == 'Selection cancelled.' || base64 == 'Camera cancelled.' || base64 === null || base64 == 'undefined' || base64.length <= 20) {
                    console.log('Sin Imagen desde Camara/Galeria');
                }
                else {
                    var obj = "data:image/jpeg;base64," + base64;
                    if (_this.form.images[0] == './assets/images/sin-imagen.png' && _this.form.images == './assets/images/sin-imagen.png') {
                        _this.form.images = [];
                        _this.form.images = [];
                    }
                    _this.form.images.push(obj);
                }
            });
        }
        else {
            alert('Alcanzo el maximo de imagenes permitidas');
        }
    };
    GenerarSiniestroPage.prototype.deleteImage = function (form, img, delIndex) {
        var _this = this;
        if (this.form.images[0] != './assets/images/sin-imagen.png') {
            var prompt_1 = this.alertCtrl.create({
                title: 'ELIMINAR IMAGEN',
                subTitle: '',
                message: '¿Esta seguro que desea eliminar la imagen seleccionada?',
                buttons: [
                    {
                        text: 'NO',
                        handler: function () {
                            console.log("Cancelamos la accion.");
                        }
                    },
                    {
                        text: 'SI',
                        handler: function () {
                            _this.form.images.splice(delIndex, 1);
                            if (_this.form.images.length == 0) {
                                _this.form.images = ['./assets/images/sin-imagen.png'];
                            }
                        }
                    }
                ]
            });
            prompt_1.present();
        }
    };
    ;
    return GenerarSiniestroPage;
}());
GenerarSiniestroPage = __decorate([
    IonicPage({
        name: 'GENERAR UN SINIESTRO',
        segment: 'GenerarSiniestroPage'
    }),
    Component({
        selector: 'page-generar-siniestro',
        templateUrl: 'generar-siniestro.html',
    }),
    __metadata("design:paramtypes", [AlertController, Service, Images, NavController, NavParams])
], GenerarSiniestroPage);
export { GenerarSiniestroPage };
//# sourceMappingURL=generar-siniestro.js.map