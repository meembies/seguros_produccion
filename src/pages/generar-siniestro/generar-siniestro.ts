import { Component } from '@angular/core';
import {  NavController, NavParams, AlertController, Platform } from 'ionic-angular';

import { Images } from '../../providers/images/images';
import { Service } from '../../providers/service/service';
//import { DomSanitizer } from '@angular/platform-browser';
import { ListaSiniestrosPage } from '../lista-siniestros/lista-siniestros';


// import { Media, MediaObject } from '@ionic-native/media';
// import { File } from '@ionic-native/file';

// declare var cordova: any;
// declare var window: any;

/**
 * Generated class for the GenerarSiniestroPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
// @IonicPage({
//   name: 'GENERAR UN SINIESTRO',
//   segment: 'GenerarSiniestroPage'
// })

@Component({
  selector: 'page-generar-siniestro',
  templateUrl: 'generar-siniestro.html',
})

export class GenerarSiniestroPage {
  name:any;
  fecha:any;
  myDate:any =  new Date().toISOString();
  form: any;
  accion: string = 'ENVIAR SINIESTRO';
  modificar: boolean = true;
  constructor(public platform: Platform, public alertCtrl:AlertController , public service: Service, public img: Images, public navCtrl: NavController, public navParams: NavParams) {
    console.log(this.myDate);
       let empresa = sessionStorage.getItem('empresa')
          this.form = {
                  fecha: this.myDate,
                  mi_nombre: '',
                  mi_email: '',
                  fecha_creado: '',
                  fecha_modificado: '',
                  empresa: empresa, 
                  nombre: '',
                  email: '',
                  telefono_01: '', 
                  telefono_02: '', 
                  lugar: '', 
                  dominio: '',
                  aseguradora: '', 
                  marca: '', 
                  modelo: '', 
                  enviado: '',
                  images: ['./assets/images/sin-imagen.png'],
                  idRemoto: '',
                  revRemoto: ''
          }

          this.platform.registerBackButtonAction(() => {
            this.backSiniestros();
          })
  

  }


  backSiniestros(){
    this.navCtrl.push(ListaSiniestrosPage,{});
  }

  ionViewDidLoad() {
    console.log(' didload GenerarSiniestroPage');

    let mailGet = localStorage.getItem('email');

    if (mailGet && mailGet != 'default@seguros_atender') {
        this.form.mi_email = mailGet;
    }
    
    let siniestro :any= this.navParams.get('item');
    console.log(siniestro)
    if (siniestro) {

          this.accion = "MODIFICAR Y ENVIAR";
          this.form = siniestro;    

    }else{
      this.modificar = false;
    }



  }

  ionViewDidEnter() {
    this.fecha = this.myDate;
    console.log(' didenter GenerarSiniestroPage');
    // this.nota = this.media.create('file.mp3');
  }
  
  ionViewDidLeave(){
      // this.nota.release();
  }

  // saveAudio(){
  //   this.nota.startRecord();
  // }
  // playAudio(){
  //   this.nota.play();
  // }
  // stopAudio(){
  //   this.nota.stopRecord();
  // }


  Save(form){
    
    let siniestro :any= this.navParams.get('item');
    console.log(siniestro)
    
    if (siniestro) {
        
     this.service.siniestros.put(form).then((res:any)=>{
         console.log('doc:')
         console.log(res)
          if (this.modificar) {
           
            let prompt = this.alertCtrl.create({
                  title: 'SINIESTRO YA ENVIADO',
                  subTitle: 'Este siniestro ya fue enviado anteriormente',
                  message: '¿Desea reenviar el siniestro nuevamente modificado?',

                  buttons: [
                      {
                      text: 'NO',
                      handler: () => {
                       console.log("Cancelamos la accion.")       
                          this.navCtrl.push(ListaSiniestrosPage)
                      }
                    },
                    {
                      text: 'SI',

                      handler: () => {
                                
                                this.Send(form)

                }
              }]
            })
                  prompt.present()

         }else{
           this.Send(form)
         }
      })


    }else{
        var date = new Date();
        let create = ( new Date() ).getTime();
        form._id = 'siniestro_'+create;
        form.type = 'siniestro';
        console.log(form.fecha);
        // form.fecha_2 =  form.fecha.split('T');
        form.fecha_2 = form.fecha;
        
        for (var i = 0; i < this.form.images.length; i++) {
          if (!this.form.images[i]) {
            this.form.images[i] = 'null'
          }
        }
        /* si hay imagenes */

        form.favorito = false;
        console.log(form);

        form.fecha_creado = create;
        
       this.service.siniestros.put(form).then((res:any)=>{
         console.log('doc Guardado Local:')
         console.log(res)
         form._id = res.id;
         form._rev = res.rev;
           this.Send(form)
      })

    }
    


  };

  delete(form){
    
        let prompt = this.alertCtrl.create({
        title: 'ELIMINAR SINIESTRO',
        subTitle: '',
        message: '¿Esta seguro que desea eliminar este siniestro?',

        buttons: [
            {
            text: 'NO',
            handler: () => {
             console.log("Cancelamos la accion.")       
            }
          },
          {
            text: 'SI',
            handler: () => {

        this.service.siniestros.get(form._id).then((doc)=>{
                          doc._deleted = true;
                       this.service.siniestros.put(doc).then((res)=>{
                           console.log(res)
                              this.navCtrl.push(ListaSiniestrosPage)
                       });

                     })
      }
    }]
  })
        prompt.present()
  }

  Send(doc){
               

                         let shot = "data:image/jpeg;base64,";
                         doc.de_email = localStorage.getItem('email');
                         doc._attachments = {};
                        
                        
                         if (!doc.empresa) {
                            doc.empresa = sessionStorage.getItem('empresa')
                         }

                        //sin-imagenes
                         if (doc.images[0]  == './assets/images/sin-imagen.png' ) {
                           doc.images = null;
                         }else{


                               if (doc.images[0] && doc.images[0].length > 10) {
                                 let foto_01 = doc.images[0].replace(shot,'');
                                 doc._attachments.foto_01 = {
                                     content_type: 'image/jpeg',
                                     data: foto_01
                                   }
                               }

                                if (doc.images[1] && doc.images[1].length > 10) {
                                 let foto_02 = doc.images[1].replace(shot,'');

                                 doc._attachments.foto_02 = {
                                     content_type: 'image/jpeg',
                                     data: foto_02
                                   }
                               }

                                if (doc.images[2] && doc.images[2].length > 10) {
                                 let foto_03 = doc.images[1].replace(shot,'');

                                 doc._attachments.foto_03 = {
                                     content_type: 'image/jpeg',
                                     data: foto_03
                                   }
                               }

                                if (doc.images[3] && doc.images[3].length > 10) {
                                 let foto_04 = doc.images[3].replace(shot,'');

                                 doc._attachments.foto_04 = {
                                     content_type: 'image/jpeg',
                                     data: foto_04
                                   }
                               }

                               if (doc.images[4] && doc.images[4].length > 10) {
                                 let foto_05 = doc.images[4].replace(shot,'');

                                 doc._attachments.foto_05 = {
                                     content_type: 'image/jpeg',
                                     data: foto_05
                                   }
                               }

                        doc.images = null;
                      }
                             
                        let ts = ( new Date() ).getTime();
                       

                        doc.fecha_modificado = ts;
                       
                        let docModificado = doc;
                        docModificado._rev = null;


                        if (this.modificar) {

                           docModificado._id = doc.idRemoto;
                           docModificado._rev = doc.revRemoto;
                        }

                      this.service.dataBase.put(docModificado).then((res:any)=>{
                         console.log('doc:')
                         console.log(res)

                         this.service.siniestros.get(doc._id).then((newDoc:any)=>{
                           newDoc.idRemoto = res.id;
                           newDoc.revRemoto = res.rev;
                           this.service.siniestros.put(newDoc).then((res)=>{
                             console.log('OK')
                             alert('SINIESTRO ENVIADO. Recuerde confirmar este siniestro.')
                             this.navCtrl.push(ListaSiniestrosPage)
                           })
                           console.log('Guardo IDS en local');
                         })


                      })

  }

 
  public addImageToSlider(){
   
    if (this.form.images.length < 5) {

        this.img.presentActionSheet().then((base64: any) => {
      

          if (base64 == 'Selection cancelled.' || base64 == 'Camera cancelled.' || base64 === null || base64 == 'undefined' || base64.length <= 20) {
           console.log('Sin Imagen desde Camara/Galeria')
         }else{              


         let obj = "data:image/jpeg;base64," + base64;
   
          

               if(this.form.images[0] == './assets/images/sin-imagen.png' && this.form.images == './assets/images/sin-imagen.png') {
                  this.form.images = [];
                  this.form.images = [];
                }

               this.form.images.push(obj)
        }               
      
      })

    }else{
      alert('Alcanzo el maximo de imagenes permitidas')
    }
    
  

  }

public deleteImage(form, img, delIndex){

if (this.form.images[0] != './assets/images/sin-imagen.png') {

        let prompt = this.alertCtrl.create({
        title: 'ELIMINAR IMAGEN',
        subTitle: '',
        message: '¿Esta seguro que desea eliminar la imagen seleccionada?',

        buttons: [
            {
            text: 'NO',
            handler: () => {
             console.log("Cancelamos la accion.")       
            }
          },
          {
            text: 'SI',
            handler: () => {

             this.form.images.splice(delIndex, 1);
           
             if (this.form.images.length == 0) {
               this.form.images =  ['./assets/images/sin-imagen.png'];
             }            

            }
         }]
       })
        prompt.present()

}

};



}  
