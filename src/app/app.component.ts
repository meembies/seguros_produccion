import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { OneSignal } from '@ionic-native/onesignal';

import { Service } from '../providers/service/service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private Service: Service,
    private oneSignal: OneSignal) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.overlaysWebView(false);
      

      if (platform.is('ios')) {
        statusBar.backgroundColorByHexString('#ffffff');
      }else{
        statusBar.backgroundColorByHexString('#393939');
      }

      splashScreen.hide();
      
      this.initNotifications();
      
    }); 
  }

  initNotifications() {
    
        let idIOS = '02888310-6c00-40f4-8544-1da4e2a95ef9';
        let idAND = '632936672059'

        this.oneSignal.startInit(idIOS, idAND);
                
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
         console.log('received')// do something when notification is received
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
          console.log('opened')// do something when a notification is opened
        });

        this.oneSignal.endInit();

     }

}
