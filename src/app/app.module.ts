import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

/* pages */
import { GenerarCotizacionPage } from '../pages/generar-cotizacion/generar-cotizacion';
import { GenerarSiniestroPage } from '../pages/generar-siniestro/generar-siniestro';
import { ListaAseguradorasPage } from '../pages/lista-aseguradoras/lista-aseguradoras';
import { ListaSiniestrosPage } from '../pages/lista-siniestros/lista-siniestros';
import { LoginPage } from '../pages/login/login';
import { MisPolizasPage } from '../pages/mis-polizas/mis-polizas';


/* providers */
import { Service } from '../providers/service/service';
import { Images } from '../providers/images/images';

/* native extras */
// import { SQLite } from '@ionic-native/sqlite';
import { Camera } from '@ionic-native/camera';
import { CallNumber } from '@ionic-native/call-number';
import { File } from '@ionic-native/file';
// import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { Network } from '@ionic-native/network';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SMS } from '@ionic-native/sms';
import { OneSignal } from '@ionic-native/onesignal';
import { FileOpener } from '@ionic-native/file-opener';
import { AndroidPermissions } from '@ionic-native/android-permissions';


@NgModule({
  declarations: [
    MyApp, 
    HomePage,
    GenerarCotizacionPage,
    GenerarSiniestroPage,
    ListaAseguradorasPage,
    ListaSiniestrosPage,
    LoginPage,
    MisPolizasPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GenerarCotizacionPage,
    GenerarSiniestroPage,
    ListaAseguradorasPage,
    ListaSiniestrosPage,
    LoginPage,
    MisPolizasPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    OneSignal,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Service,
    Images,
    Camera,
    CallNumber,
    InAppBrowser,
    SocialSharing,
    FilePath,
    FileOpener,
    SMS,
    DocumentViewer,
    File,
    Network,
    AndroidPermissions
  ]
})
export class AppModule {}
