var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable, NgZone } from '@angular/core';
import PouchDB from 'pouchdb';
PouchDB.debug.disable();
var Service = (function () {
    function Service(zone) {
        this.zone = zone;
        this.aseguradora = 'seguros_carlitos';
        //,{adapter:'websql'}
        //default@seguros.com.ar
        this.dataBase = new PouchDB("bd_seguros_010101", { adapter: 'websql' });
        //http://meemba.com.ar:5984/_utils/database.html?seguros_app_02
        this.siniestros = new PouchDB("siniestros3352", { adapter: 'websql' });
        this.username = 'seguros';
        this.password = 's3gur0s';
        this.remote = 'http://www.meemba.com.ar:5984/seguros_app_02';
        var email = localStorage.getItem('email');
        //Por email
        var base = 'default@' + this.aseguradora;
        if (email && email != base) {
            console.log('bdPoremail');
            this.applyTo();
            this.applyFrom(email);
            //Por Default;
        }
        else {
            if (!email) {
                console.log('seteoLocalStorage');
                email = 'default@' + this.aseguradora;
                localStorage.setItem('default', email);
                localStorage.setItem('email', email);
            }
            console.log('bdPorDefault');
            this.applyTo();
            this.applyFrom(email);
        }
    }
    Service.prototype.destroy = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.dataBase.destroy().then(function (res) {
                resolve('OK');
            });
        });
    };
    Service.prototype.applyFrom = function (email) {
        var _this = this;
        return new Promise(function (resolve) {
            var opciones = {
                live: true,
                retry: true,
                continuos: true,
                doc_ids: [email, _this.aseguradora],
                filter: '_view',
                view: 'app/datos',
                auth: {
                    username: _this.username,
                    password: _this.password
                }
            };
            _this.dataBase.replicate.from(_this.remote, opciones).on('change', function (info) {
                console.log("changeFrom DB");
            }).on('paused', function (err) {
                console.log("pausedFrom  DB");
            }).on('active', function (info) {
                console.log("activedFrom  DB");
            }).on('complete', function (info) {
                console.log('complete');
            });
            resolve('OK');
        });
    };
    Service.prototype.applyTo = function () {
        var opciones = {
            live: true,
            retry: true,
            continuos: true,
            auth: {
                username: this.username,
                password: this.password
            }
        };
        this.dataBase.replicate.to(this.remote, opciones).on('change', function (info) {
            console.log("changeTo DB");
        }).on('paused', function (info) {
            console.log("pausedTo DB");
        }).on('active', function (info) {
            console.log("activedTo DB");
        });
    };
    /*HANDLECHANGE*/
    Service.prototype.handleChange = function (change) {
        var _this = this;
        this.zone.run(function () {
            var changedDoc = null;
            var changedIndex = null;
            _this.data.forEach(function (doc, index) {
                if (doc._id === change.id) {
                    changedDoc = doc;
                    changedIndex = index;
                }
            });
            //A document was deleted
            if (change.deleted) {
                _this.data.splice(changedIndex, 1);
            }
            else {
                //A document was updated
                if (changedDoc) {
                    _this.data[changedIndex] = change.doc;
                }
                else {
                    _this.data.push(change.doc);
                }
            }
        });
    };
    /*GET POUCH dataBase OF PRODUCTOS */
    /*
    getTypeDocuments(doc){
      console.log("aca")
      return new Promise(resolve => {
          console.log(doc)
    
        this.dataBase.createIndex({
          index: {
            fields: ['doc_tipo']
          }
        })
        
    
        this.dataBase.find({
          selector: {doc_tipo: doc},
        }).then(function (result) {
          console.log(result)
          resolve(result)
        }).catch(function (err) {
          resolve(err)
        })
          
    })
    }
    */
    Service.prototype.getSiniestros = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siniestros.allDocs({
                include_docs: true
            }).then(function (result) {
                console.log(result);
                _this.dataSiniestros = [];
                result.rows.map(function (row) {
                    _this.dataSiniestros.push(row.doc);
                    resolve(_this.dataSiniestros);
                });
                /*
                        this.dataBase.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
                          this.handleChange(change);
                        });
                */
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    /*GET ALL Documents*/
    Service.prototype.getDocuments = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.dataBase.allDocs({
                include_docs: true
            }).then(function (result) {
                console.log(result);
                _this.data = [];
                result.rows.map(function (row) {
                    _this.data.push(row.doc);
                    resolve(_this.data);
                });
                _this.dataBase.changes({ live: true, since: 'now', include_docs: true }).on('change', function (change) {
                    _this.handleChange(change);
                });
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    Service.prototype.getAttachments = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.dataBase.getAttachment(id).then(function (blob) {
                //var url = URL.createObjectURL(blob)
                resolve(blob);
            }).catch(function (err) {
                resolve(err);
            });
        });
    };
    /*GET images by ID-NOMBRE*/
    Service.prototype.getImage = function (id, nombre) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.dataBase.getAttachment(id, nombre).then(function (blob) {
                var url = URL.createObjectURL(blob);
                resolve(url);
            }).catch(function (err) {
                resolve(err);
            });
        });
    };
    /*get by ID*/
    Service.prototype.getDocumentByID = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.dataBase.get(id).then(function (res) {
                // handle results
                resolve(res);
            }).catch(function (err) {
                // handle error
            });
        });
    };
    /*POST-PUT documentGral*/
    Service.prototype.putDocument = function (doc) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.dataBase.put(doc).then(function (res) {
                resolve(res);
            }).catch(function (error) {
                console.log("error");
                console.log(error);
            });
        });
    };
    return Service;
}());
Service = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [NgZone])
], Service);
export { Service };
//# sourceMappingURL=service.js.map