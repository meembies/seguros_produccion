import { Injectable, NgZone } from '@angular/core';
import PouchDB from 'pouchdb';
// PouchDB.debug.disable();
PouchDB.plugin(require('pouchdb-adapter-websql'));

@Injectable()
export class Service {

  dataBase: any;
  siniestros: any;

  username: string;
  password: string;

  aseguradora:string = 'seguros_atender'
  form: any;

  remote: string;

  data: any;
  dataSiniestros: any;
  
  constructor(public zone: NgZone) {
            //,{adapter:'websql'}
            //default@seguros.com.ar
            this.dataBase = new PouchDB("bd_seguros_atender_v103");
            //http://meemba.com.ar:5984/_utils/database.html?seguros_app_02
            this.siniestros = new PouchDB("atender_siniestros_v103");
            //,{adapter:'websql'}
            console.log(this.dataBase.adapter); // prints 'idb'

            this.username = 'seguros'
            this.password = 's3gur0s'
            
            this.remote = 'http://www.meemba.com.ar:5984/seguros_app_02'

            let id_user = localStorage.getItem('id_user');   
             
             //Por email
             let base = 'default@'+this.aseguradora;
             if ( id_user && id_user != base ) {
               console.log('bdPorid_user')
               this.applyTo();
               this.applyFrom(id_user, false);

             //Por Default;
             }else{

               if (!id_user) {
                 console.log('seteoLocalStorage')
                 id_user = 'default@'+this.aseguradora;
                 localStorage.setItem('default', id_user);
                 localStorage.setItem('email', id_user);
                 localStorage.setItem('id_user', id_user);
               }
               
               console.log('bdPorDefault')
               this.applyTo();
               this.applyFrom(id_user, false);
             }
              
  }

destroy(){
  return new Promise(resolve => {
    this.dataBase.destroy().then((res)=>{
      resolve('OK')
    })
  })
}
applyFrom(id_user, login){
  console.log(id_user);
  console.log('id_user')
  return new Promise(resolve => {
  let opciones = {
              live:true,
              retry:true,
              continuos:true,
              doc_ids:[id_user, this.aseguradora],
              filter: '_view',
              view: 'app/datos',
              auth: {
                username: this.username,
                password: this.password
              }
            }
   
   console.log(opciones);
   
   this.dataBase.replicate.from(this.remote, opciones).on('change', function (info) {
                console.log("changeFrom DB")
              }).on('paused', function (err) {
                localStorage.setItem('db','seteada');
                 if(login){
                   // this.dataBase.getDocuments().then((docs)=>{
                   //   console.log('docs->')
                   //   console.log(docs)
                   // })
                     setTimeout(()=>{
                      resolve('OK')
                     }, 3000)
                  }
                console.log("pausedFrom  DB")
              }).on('active', function (info) {
                console.log("activedFrom  DB")
              }).on('complete', function(info){
                console.log('complete');
              })

              if (!login) {
                resolve('OK');
              } 
      
    })
}


applyTo(){
  let opciones = {
              live:true,
              retry:true,
              continuos:true,
              auth: {
                username: this.username,
                password: this.password
              }
            }

              this.dataBase.replicate.to(this.remote, opciones).on('change', function (info) {
                console.log("changeTo DB")
              }).on('paused', function (info) {
                console.log("pausedTo DB")
              }).on('active', function (info) {
                console.log("activedTo DB")
              })
}

/*HANDLECHANGE*/
handleChange(change){

    this.zone.run(() => {

      let changedDoc = null;
      let changedIndex = null;

      this.data.forEach((doc, index) => {

        if(doc._id === change.id){
          changedDoc = doc;
          changedIndex = index;
        }

      });

      //A document was deleted
      if(change.deleted){
        this.data.splice(changedIndex, 1);
      }
      else {

        //A document was updated
        if(changedDoc){
          this.data[changedIndex] = change.doc;
        }
        //A document was added
        else {
          this.data.push(change.doc);
        }

      }

    });

  }

/*GET POUCH dataBase OF PRODUCTOS */

/*
getTypeDocuments(doc){
  console.log("aca")
  return new Promise(resolve => {
      console.log(doc)

    this.dataBase.createIndex({
      index: {
        fields: ['doc_tipo']
      }
    })
    

    this.dataBase.find({
      selector: {doc_tipo: doc},
    }).then(function (result) {
      console.log(result)
      resolve(result)
    }).catch(function (err) {
      resolve(err)
    })
      
})
}
*/


getSiniestros() {
return new Promise(resolve => {

      this.siniestros.allDocs({
        include_docs: true
      }).then((result) => {
        console.log(result)
        this.dataSiniestros = [];

       result.rows.map((row) => {
          this.dataSiniestros.push(row.doc);
          resolve(this.dataSiniestros);
        });

/*
        this.dataBase.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
          this.handleChange(change);
        });
*/
      }).catch((error) => {
        console.log(error);
      });

    });

}

/*GET ALL Documents*/
getDocuments() {
return new Promise(resolve => {

      this.dataBase.allDocs({
        include_docs: true
      }).then((result) => {
        console.log(result)
        this.data = [];

       result.rows.map((row) => {
          this.data.push(row.doc);
          resolve(this.data);
        });

        this.dataBase.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
          this.handleChange(change);
        });

      }).catch((error) => {
        console.log(error);
      });

    });

}

getAttachments(id){
    return new Promise(resolve => {
   this.dataBase.getAttachment(id).then((blob) => {  
       //var url = URL.createObjectURL(blob)
       resolve(blob)
    }).catch((err)  => { 
      resolve(err);
    });

    })
}
/*GET images by ID-NOMBRE*/
getImage(id,nombre){
   return new Promise(resolve => {
   this.dataBase.getAttachment(id, nombre).then((blob) => {  
       var url = URL.createObjectURL(blob)
       resolve(url)
    }).catch((err)  => { 
      resolve(err);
    });

    })
}


getAttachmentPdf(id, nombre){
    return new Promise(resolve => {
    this.dataBase.getAttachment(id, nombre).then((data) => {  
       
       resolve(data);

    }).catch((err)  => { 
      resolve(err);
    });

    })
}

/*get by ID*/
getDocumentByID(id){
  return new Promise(resolve => {
    this.dataBase.get(id).then((res)  =>{
      // handle results
      resolve(res);
    }).catch((err) => {
      // handle error
      console.log(err);
    }); 
})
}

/*POST-PUT documentGral*/
putDocument(doc){
        return new Promise(resolve => {
             this.dataBase.put(doc).then( (res) =>{
              resolve(res)
            }).catch((error) => {
              console.log("error");
              console.log(error);
            }); 
        })
  }


/*Funciones Privadas*/


}
