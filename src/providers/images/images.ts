import { Injectable } from '@angular/core';
import { ActionSheetController, ToastController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

//import { File } from '@ionic-native/file';
//import { FilePath } from '@ionic-native/file-path';






@Injectable()
export class Images {

  constructor(public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, private camera: Camera) {}
 public presentActionSheet() {
        return new Promise(resolve => {

          let actionSheet = this.actionSheetCtrl.create({
            title: 'Seleccionar Imagen',
            buttons: [
              {
                text: 'Desde la galería',
                handler: () => {
                  this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY).then((res) => {
                     resolve(res)
                  });

                }
              },
              {
                text: 'Utilizar la Camara',
                handler: () => {
                  this.takePicture(this.camera.PictureSourceType.CAMERA).then((res) => {
                     resolve(res)
                  });

                }
              },
              {
                text: 'Cancelar',
                role: 'cancel'
              }
            ]
          });
       
       actionSheet.present();

     })
  }

 private takePicture(sourceType){
     return new Promise(resolve => {
       console.log('Parametros Imagen:');
       console.log(sourceType);
   
     this.camera.getPicture({
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 640,
        saveToPhotoAlbum: true,
        quality: 80,
        targetHeight: 480
    }).then((imageData) => {
      // imageData is a base64 encoded string
        resolve(imageData)
    }, (err) => {
        resolve(err);
        this.presentToast(err)
    });
 
    
    })
 }
   
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
   

}

