var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { ActionSheetController, ToastController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
//import { File } from '@ionic-native/file';
//import { FilePath } from '@ionic-native/file-path';
var Images = (function () {
    function Images(actionSheetCtrl, toastCtrl, camera) {
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.camera = camera;
    }
    Images.prototype.presentActionSheet = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var actionSheet = _this.actionSheetCtrl.create({
                title: 'Seleccionar Imagen',
                buttons: [
                    {
                        text: 'Desde la galería',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY).then(function (res) {
                                resolve(res);
                            });
                        }
                    },
                    {
                        text: 'Utilizar la Camara',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.CAMERA).then(function (res) {
                                resolve(res);
                            });
                        }
                    },
                    {
                        text: 'Cancelar',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        });
    };
    Images.prototype.takePicture = function (sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            console.log('Parametros Imagen:');
            console.log(sourceType);
            _this.camera.getPicture({
                sourceType: sourceType,
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 640,
                saveToPhotoAlbum: true,
                quality: 80,
                targetHeight: 480
            }).then(function (imageData) {
                // imageData is a base64 encoded string
                resolve(imageData);
            }, function (err) {
                resolve(err);
                _this.presentToast(err);
            });
        });
    };
    Images.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    return Images;
}());
Images = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [ActionSheetController, ToastController, Camera])
], Images);
export { Images };
//# sourceMappingURL=images.js.map