
# Seguros

Actualización de grupo de seguros en base a politicas de android. 
Atender, Caeiro, Castro, Maidana

## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#getting-started)Getting Started

Master en base a branch atender, para replicar proyecto, descargar instalar ionic plugins y sus dependencias npm.

### [](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#prerequisites)Prerequisites

```
Ionic Framework v3

```

## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#built-with)Built With

-   [Ionic v3]([https://ionicframework.com/docs/v3/](https://ionicframework.com/docs/v3/))  - The web framework used

## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#contributing)Contributing

Meemba Software SRL.

## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#versioning)Versioning

Version 1.1 Android
## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#authors)Authors

-   **Matias Cristiani**  -  _Initial work_  -  [webmeemba]([https://github.com/webmeemba](https://github.com/webmeemba))


## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#license)License


## [](https://gist.github.com/webmeemba/109311bb0361f32d87a2#acknowledgments)Acknowledgments

-   etc
